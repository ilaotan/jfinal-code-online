package com.jfinal.codeonline.core;

public class Config {

    private static IConfigDataProvider configDataProvider;

    private static String templatePath;

    private static String targetPath;

    private static IScriptHelper scriptHelper;

    private static ITemplateHelper templateEngine;

    private Config(){

    }

    public static IConfigDataProvider configDataProvider() {
        return configDataProvider;
    }

    public static void configDataProvider(IConfigDataProvider configDataProvider) {
        Config.configDataProvider = configDataProvider;
    }

    public static String templatePath() {
        return templatePath;
    }

    public static void templatePath(String templatePath) {
        Config.templatePath = templatePath;
    }

    public static String targetPath() {
        return targetPath;
    }

    public static void targetPath(String targetPath) {
        Config.targetPath = targetPath;
    }

    public static IScriptHelper scriptHelper() {
        return scriptHelper;
    }

    public static void scriptHelper(IScriptHelper scriptHelper) {
        Config.scriptHelper = scriptHelper;
    }

    public static ITemplateHelper templateEngine() {
        return templateEngine;
    }

    public static void templateEngine(ITemplateHelper templateEngine) {
        Config.templateEngine = templateEngine;
    }
}